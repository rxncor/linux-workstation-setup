
echo "provision user source files"

source ./ansiblepy/bin/activate
AP=ansible_provisioning

echo "install vim-plug and sensible template vimrc"
ansible-playbook $AP/vim.yml -i $AP/HOSTS --ask-sudo-pass  --module-path $AP/ansible_module


echo "install dotfiles"
ansible-playbook $AP/dotfiles.yml -i $AP/HOSTS --ask-sudo-pass  --module-path $AP/ansible_module 


