
echo "executing playbooks with --check and --diff flags"
echo "This is a dryrun, this may not be apparent as the output will be as per usual"
echo "some commands may not supported in check mode and will cause ansible to fail the task"
echo "Workaround is to use --skip-tags="", for example --skip-tags="yumdownloadonly" "

sed 's/^ansible-playbook.*$/\0 --check --diff --skip-tags=\"yumdownloadonly\"/' provision.sh > .dryrun-provision.sh
sed 's/^ansible-playbook.*$/\0 --check --diff/' provision-user-preferences.sh > .dryrun-provision-user-preferences.sh

chmod +x .provision-dryrun.sh .provision-user-preferences-dryrun.sh 

. .dryrun-provision.sh 
. .dryrun-provision-user-preferences.sh  

