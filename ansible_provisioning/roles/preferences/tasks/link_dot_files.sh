# link home directory - includes .gemrc .rvmrc .zshrc bin/* 
function link_homedir_files () {
  for file in $1/*; do
    if [[ -d $file ]]; then 
      mkdir -p $2/`basename $file`
      link_homedir_files $file $2/`basename $file`
    else
      ln -f $file $2/`basename $file`
    fi
  done
}
shopt -s dotglob
link_homedir_files ~/dot-files/home ~
shopt -u dotglob
