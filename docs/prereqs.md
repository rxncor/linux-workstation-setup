
# Ansible prereqs

I like virtualenvs, so I am going to install ansible in a virtualenv.
This also requires the internet :|

# Install prereqs
As a user with sudo permissions -
. ./configure.sh

# Notes

Requirements
- python
- python-setuptools
- python-pip
- python-virtualenv or pip install virtualenv

Create virtualenvironment
virtualenv ansiblepy
ansiblepy/bin/pip install --upgrade pip
ansiblepy/bin/pip install ansible

## load the virtualenv
Note you will have to repeat this step everytime you want to use ansible. (It   is possible to add to your bashrc but that is dangerous, as who knows how many  virtual environments you have and which you tend to use)

    source ansiblepy/bin/activate

