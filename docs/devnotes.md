
Ansible layout

./ansible_provisioning

setup.yml                 # master playbook
HOSTS                     # HOSTS file which defines where the script will run. This points to localhost.
requirements.txt          # the requirements for running this playbook, including ansible. Not using this at the moment.
run.sh                    # one command script for install requirements, cloning the repo and provisioning.

roles/
    common/               # this hierarchy represents a "role"
        tasks/            #
            *.yml         #  <-- tasks files i'll talk about later
        files/            #
            *.*           #  <-- files for use with the copy resource
        vars/             #
            main.yml      #  <-- variables associated with this role



things I want to include , probably pull out common into separate sections so that this is more reusable
- repos_and_yum_related
yum_conf_epel yum_conf_adobe? + other exotics for instance ius see (docs/references.md)
development_compilers_and_coding
- gcc gcc-c++ gcc-gfortran gdb
- blas  blas-devel lapack lapack-devel numpy openmpi
- git gitk mercurial
- tmux
- autojump
- environment_modules
- ntfsprogs ntfs-3g ntfs-3g-devel

ipynb?? 

local installs e.g. vmd, maple, matlab, pgi (perhaps give people perms to install and access by local share) this will ease admin burden alot as people can just get the latest install details from git and install themselves


# issues with yum update

this seems to hang and then eventually die. Related to remote repos being slow/down. I have forced ftp.leg usage and still it is slow. 
A yum update outside ansible seems ok. but then I broke everything with crtl-C

## to fix this broken update
think about using distro-sync in the future.

tried http://forums.fedoraforum.org/showthread.php?t=194254
yum clean all
yum makecache
rpm --rebuilddb
yum update

still issues
tried https://destefano.wordpress.com/2013/08/13/yum-update-fail/
sudo yum check all                # tells you of any problems
sudo package-cleanup --problems   # lists all known package problems
sudo package-cleanup --dupes      # lists duplicate packages
sudo package-cleanup --cleandupes # actually cleans up duplicates
sudo yum check all                # run again to check for remaining problems
sudo yum-complete-transaction --cleanup-only


### problems 
sudo package-cleanup --problems   # lists all known package problems
Package 1:java-1.8.0-openjdk-devel-1.8.0.65-2.b17.el7_1.x86_64 has missing requires of java-1.8.0-openjdk = ('1', '1.8.0.65', '2.b17.el7_1')

### dupes 
sudo package-cleanup --dupes      # lists duplicate packages
way too may
sudo package-cleanup --cleandupes # actually cleans up duplicates

but this wants to Remove  56 Packages (+625 Dependent packages)

Installed size: 2.3 G
??
n for now
did a yum list and looked through all packages. duplicate looking packages from @anaconda vs those from sl/epel. These are clearly from the broken update
so say yes. (Do not try this)
A really bad idea. This crashed X and broke everything. luckily on reboot I got text access... 

decided to try a yum-complete-transaction
then 
Yum check all , breaks
Yum remove java-1.8.0-openjdk-devel-…
Rerun check

On reboot
Major systemm failure
Log in to emergency mode
Can't access root
Journalctl -xb
Dependency failed for local file systems.. 
lol

### Reinstall workstation. 
For my machine, make sure to use latest SL7 iso or it all goes bananas. Will still have to install using basic graphics mode.
Choose dev workstation. 
Once OS is up, get latest NVIDIA driver this is (on 6/11/2015) the following -> wget http://us.download.nvidia.com/XFree86/Linux-x86_64/352.55/NVIDIA-Linux-x86_64-352.55.run
init 1; chmod +x NVIDI*.run; ./NVI*.run ; init 5


# modifying update

I initially thought distro-sync via command. 
but this is also prone to breaking
- name: upgrade all packages using distro sync via command (may take a while)
  sudo: yes
  command: yum distribution-synchronization -y
  register: yum_result

- name: Debug yum distro-sync
  debug: "msg= yum dist sync stdout was `{{ yum_result.stdout }}`"

and there is a bug in this anyway (the debug part breaks)


I think the best solution may be to download all packaged then separately apply the upgrade.  see here https://access.redhat.com/solutions/10154
less risk form faulty network services. less risk as less time spent updating. 
- name: download all packages for update (may take hours)
  sudo: yes
  command: yum update --downloadonly -y
  register: yum_result
  tags:
   - yumdownloadonly

- debug: var=yum_result # prints out info about command result
  tags:
   - yumdownloadonly

- fail: msg="yum error"  # if yum update download has anything in stderr then fail
  when: yum_result.stderr!=""
  tags:
   - yumdownloadonly

- name: yum update check with ansible (may take hours)
  sudo: yes
  yum: name=* state=latest --downloadonly
  tags:
   - cantakeawhile


if possible avoid a kernel update


