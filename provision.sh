
# Provision using ansible
echo "provision with ansible"
source ./ansiblepy/bin/activate
AP=ansible_provisioning

# example with additional vars
#ansible-playbook $AP/setup.yml -i $AP/HOSTS --ask-sudo-pass  --module-path $AP/ansible_module --tags "debug" --extra-vars "use_ftp_leg=True"

# example using tags
#ansible-playbook $AP/setup.yml -i $AP/HOSTS --ask-sudo-pass  --module-path $AP/ansible_module --tags "repo"

ansible-playbook $AP/setup.yml -i $AP/HOSTS --ask-sudo-pass  --module-path $AP/ansible_module 
