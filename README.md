

# Basic Mission

Following in the footsteps of the Patrick Wyatt and many others... [references](docs/references.md) I am to automate setup of my linux workstation.

I use Scientific Linux so things will be geared around this OS.

## Future ideas
Expand this to assist in automated user general user setup. 
include some kind of testing. see http://docs.ansible.com/ansible/test_strategies.html


# Install pattern

- install prereqs for ansible [ansible_prerequisites](docs/prereqs.md)
- use ansible to run playbooks that install's everything (i.e. source dotfile repo, install all sorts of dependencies) 
- use ansible to get dotfiles and all that stuff. This is separate from the standard provision to avoid mishaps.

# Installation

- Make sure you have ssh-keys for bitbucket
- clone the repo git clone git@bitbucket.org:rxncor/linux-workstation-setup.git
- cd linux-workstation-setup
- . ./configure.sh 
- . ./provision.sh
- . ./provision-user-preferences.sh # (for the current user), (a multiuser workaround maybe to sudo -u $USER (untested))

# Dry run installation

Nervous? Then run ansible in check mode. use --check and --diff flags. http://docs.ansible.com/ansible/playbooks_checkmode.html
These flags must be added to each ansible_playbook statement. This process is automated via sed statements in dryrun.sh
so :
./dryrun.sh

Note this will look realistic although it is a dryrun.

# Installation issues

Where possible read the errors from ansible. Common issues so far are:

- sudo perms not set up correctly
- yum not contacting mirrors. Check internet and make sure to use ftp.leg.uct.ac.za where necessary
- trying to install packages that do not exist for this release




# Manually installed software

##- VMware Workstation Player
Current version from https://my.vmware.com/web/vmware/free#desktop_end_user_computing/vmware_workstation_player/12_0 VMware Workstation 12.0.1 Player for Linux 64-bit


###  Bugs
I/O warning : failed to load external entity "/etc/vmware/hostd/proxy.xml
seems related to libicu. Make sure this updates and installed. 
also for the first time, must input an email address. 
After this vmplayer -H is fine. 

## Dev
Dev notes are here [devnotes](docs/devnotes.md)

## 

