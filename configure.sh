#!/usr/bin/env bash
# By Chris Barnett 30/10/2015
# Install the prerequisites to get ansible working (then use ansible to provision)


# Make sure this script is not run with sudo
if [ $(id -u) -eq 0 ]
then
  echo 'ERROR: This script should not be run as sudo or root.'
  exit
fi

# install EPEL first as python-pip is in EPEL. only install epel-release not yum-conf. yum-conf-epel installs fastest mirrors which tends to break.
sudo yum install epel-release


# Install useful packages (requirements for ansible)
for package in python python-setuptools python-pip 
do
sudo yum install -y $package
done

# Install virtualenv
sudo pip install virtualenv    

# Create virtualenv, update pip and install ansible
virtualenv ansiblepy
ansiblepy/bin/pip install --upgrade pip
ansiblepy/bin/pip install ansible

# Completion message
echo "Basic configuration complete"
echo "a functional ansible virtualenv should now be available at ./ansiblepy"
